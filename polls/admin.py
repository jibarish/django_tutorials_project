from django.contrib import admin
from polls.models import Poll, Choice 

class ChoiceInline(admin.TabularInline):   # use StackedInline for more spread
    model = Choice
    extra = 3

class PollAdmin(admin.ModelAdmin):
    # Change the order of the fields
    # fields = ['pub_date', 'question']

    # Split the form up into fieldsets
    fieldsets = [
        (None,             {'fields': ['question']}),
        # This fieldset is initially collapsed
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    # Display each Poll's associated Choices
    inlines = [ChoiceInline]
    # Display more info about each Poll in the Polls list change view
    list_display = ('question', 'pub_date', 'was_published_recently')
    # Add filtering option
    list_filter = ['pub_date']
    # Add search capability
    search_fields = ['question']
    # Add hierarchical navigation by date (drill down)
    date_hierarchy = 'pub_date'

admin.site.register(Poll, PollAdmin)
